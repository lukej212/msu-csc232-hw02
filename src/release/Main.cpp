/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Luke Johnson <edward212@live.missouristate.edu>        
 *          
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

        /**
         * @brief Gets the variables m and n.
         * @return The output of Ackermann's function.
         */
		int acker(int, int);

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {

    std::cout << "Acker(1, 2) = " << acker(1, 2)
              << std::endl;
    return EXIT_SUCCESS;
}

   		int acker(int m, int n) {
			if (m == 0){
				return n + 1;
			}
			else if (n == 0) {
				return acker(m - 1, 1);
			}
			else {
				return acker(m - 1, (acker(m, n - 1)));
		    }
        } 


