# Unix Makefile Generator

To generate a Unix Makefile for this project, build the executable and run it, type the following commands at the command line prompt while in this sub-directory (`build/unix`):

```bash
$ cmake -G "Unix Makefiles" ../..
-- The C compiler identification is ...
-- The CXX compiler identification is ...
-- Check for working C compiler: ...
-- Check for working C compiler: ... -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: ...
-- Check for working CXX compiler: ... -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: ...
$ make
Scanning dependencies of target HW02_Test
[ 25%] Building CXX object CMakeFiles/HW02_Test.dir/src/debug/UnitTestRunner.cpp.o
[ 50%] Linking CXX executable HW02_Test
[ 50%] Built target HW02_Test
Scanning dependencies of target HW02
[ 75%] Building CXX object CMakeFiles/HW02.dir/src/release/Main.cpp.o
[100%] Linking CXX executable HW02
[100%] Built target HW02
$ ./HW02
Acker(1, 2) = *** replace me with call to Acker***
$
```

Note: a number of `...` are shown in the above output as they are machine dependent.

After you make changes to `src/release/Main.cpp`, you can rebuild your executable with the following command:

```bash
$ make clean && make
[ 25%] Building CXX object CMakeFiles/HW02_Test.dir/src/debug/UnitTestRunner.cpp.o
[ 50%] Linking CXX executable HW02_Test
[ 50%] Built target HW02_Test
[ 75%] Building CXX object CMakeFiles/HW02.dir/src/release/Main.cpp.o
[100%] Linking CXX executable HW02
[100%] Built target HW02
$
```

And now you can execute the program as before (`./HW02`)
